package com.waaangyi.leveldisplay.api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.waaangyi.leveldisplay.LevelDisplay;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.UUID;

public class Api {
    final private String BASE_URL = "https://api.hypixel.net/";
    private String apiKey;
    private String failReason;
    private boolean fail;
    private ApiQueue apiQueue;

    public Api() {
        this.apiKey = LevelDisplay.getApiKey();
        this.apiQueue = new ApiQueue();
    }

    public String getFailReason() {
        return this.failReason;
    }

    public boolean getFail() {
        return this.fail;
    }

    private JsonObject getRequest(String function, String args) {
        this.failReason = "None";
        this.fail = false;
        try {
            if (!this.apiQueue.check()) {
                this.fail = true;
                this.failReason = "API limit reached";
                return null;
            }
            URL url = new URL(BASE_URL + function + "?key=" + this.apiKey + args);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

            this.apiQueue.add();

            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String nextLine;
                while ((nextLine = reader.readLine()) != null) {
                    sb.append(nextLine);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            String response = sb.toString();
            JsonObject jsonObject = convertJson(response);

            this.fail = checkFail(jsonObject);
            if (this.fail)
                return null;
            return jsonObject;
        } catch (Exception e) {
            this.failReason = "Connection Error";
            this.fail = true;
            e.printStackTrace();
        }
        return null;
    }

    private JsonObject convertJson(String response) {
        JsonElement jsonElement = new JsonParser().parse(response);
        return jsonElement.getAsJsonObject();
    }

    private boolean checkFail(JsonObject jsonObject) {
        try {
            if (jsonObject.get("success").getAsBoolean()) {
                return false;
            }
            this.failReason = "Success key is false";
        } catch (Exception e) {
            this.failReason = "Unknown Error, check log";
            e.printStackTrace();
        }
        return true;
    }

    public String getLevel(UUID uuid) {
        // Check for Cache
        int c = LevelDisplay.getCache().get(uuid);
        if (c == -1) {
            this.fail = true;
            this.failReason = "Unknown Player";
            return "-1";
        }
        if (c > 0) {
            this.fail = false;
            return Integer.toString(c);
        }

        final String FUNCTION = "player";
        String args = "&uuid=" + uuid.toString();
        try {
            JsonObject jsonObject = getRequest(FUNCTION, args);
            if (this.fail) {
                // Check in cache for any old data
                c = LevelDisplay.getCache().get(uuid, true);
                if (c > 0) {
                    this.fail = false;
                    return Integer.toString(c);
                }
                if (this.failReason.equals("API limit reached")) {
                    LevelDisplay.getCache().set(uuid, 0);
                }
                return this.failReason;
            }
            String networkExpRaw = jsonObject.get("player").getAsJsonObject().get("networkExp").getAsString();
            double networkExp = Double.parseDouble(networkExpRaw);
            long convertedExp = (long) (1 - 3.5 + Math.sqrt(12.25 + .0008 * networkExp));
            // Update the cache
            LevelDisplay.getCache().set(uuid, (int) convertedExp);
            return Long.toString(convertedExp);
        } catch (Exception e) {
            this.fail = true;
            this.failReason = "Unknown Player";
        }
        return this.failReason;
    }

    public int getAvailableCount() {
        return this.apiQueue.getAvailableCount();
    }
}
