package com.waaangyi.leveldisplay.config;

public class LevelDisplayConfig {
    private String apiKey;

    public LevelDisplayConfig() {
    }

    public LevelDisplayConfig(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
