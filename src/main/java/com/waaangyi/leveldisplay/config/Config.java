package com.waaangyi.leveldisplay.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Config {
    private File configFile;
    private LevelDisplayConfig config;

    public Config(File configFile) {
        this.configFile = configFile;
    }

    public void loadConfig() {
        JsonParser parser = new JsonParser();
        Gson gson = new Gson();
        try {
            String json = FileUtils.readFileToString(this.configFile);
            this.config = gson.fromJson(json, LevelDisplayConfig.class);
            return;
        } catch (FileNotFoundException e) {
            System.out.println("leveldisplay config file does not exist, creating...");
        } catch (IOException e) {
            System.out.println("Could not read file");
        } catch (Exception e) {
            e.printStackTrace();
        }
        createConfig();
    }

    public String getApiKey() {
        return this.config.getApiKey();
    }

    public void setApiKey(String apiKey) {
        this.config.setApiKey(apiKey);
        saveConfig();
    }

    private void saveConfig() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(this.config);
        try {
            FileUtils.writeStringToFile(this.configFile, json);
        } catch (IOException e) {
            System.out.println("Could not write to file");
        }
    }

    private void createConfig() {
        LevelDisplayConfig config = new LevelDisplayConfig();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(config);
        try {
            FileUtils.writeStringToFile(this.configFile, json);
            this.config = gson.fromJson(json, LevelDisplayConfig.class);
        } catch (IOException e) {
            System.out.println("Could not write to file");
        }
    }
}
