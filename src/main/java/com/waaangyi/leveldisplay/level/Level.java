package com.waaangyi.leveldisplay.level;

import com.mojang.authlib.GameProfile;
import com.waaangyi.leveldisplay.LevelDisplay;
import com.waaangyi.leveldisplay.var.Pair;
import net.minecraft.client.Minecraft;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class Level implements Runnable {
    private final ICommandSender sender;

    public Level(ICommandSender sender) {
        this.sender = sender;
    }

    public Level() {
        this.sender = Minecraft.getMinecraft().thePlayer;
    }

    public void run() {
        List<EntityPlayer> playerList = new ArrayList<EntityPlayer>(Minecraft.getMinecraft().theWorld.playerEntities);
        playerList.remove(Minecraft.getMinecraft().thePlayer);
        sender.addChatMessage(new ChatComponentText("Attempting to get player levels..."));
        sender.addChatMessage(new ChatComponentText("There are " + playerList.size() + " players"));
        sender.addChatMessage(new ChatComponentText(LevelDisplay.getApi().getAvailableCount() + " available API calls left"));
        ArrayList<Pair<Integer, String>> playerPair = new ArrayList<Pair<Integer, String>>();
        /*
        §c - Probably Watchdog
        §k - Obfuscated Player
         */
        int successCount = 0;
        int ignoreCount = 0;
        for (EntityPlayer player : playerList) {
            try {
                GameProfile playerProfile = player.getGameProfile();
                String name = player.getDisplayName().getFormattedText();
                if (name.contains("§c")) {
                    ++ignoreCount;
                    continue;
                }
                if (name.contains("§k")) {
                    name = player.getName();
                }
                String level = LevelDisplay.getApi().getLevel(EntityPlayer.getUUID(playerProfile));
                if (!LevelDisplay.getApi().getFail()) {
                    playerPair.add(new Pair<Integer, String>(Integer.parseInt(level), name));
                    ++successCount;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        sender.addChatMessage(new ChatComponentText("Successfully loaded " + successCount + " player's level out of " + playerList.size() + " and ignored " + ignoreCount));

        Collections.sort(playerPair, new Comparator<Pair>() {

            @Override
            public int compare(Pair o1, Pair o2) {
                Integer a = (Integer) o1.getKey();
                Integer b = (Integer) o2.getKey();
                return a.compareTo(b);
            }
        });
        for (Pair player : playerPair) {
            sender.addChatMessage(new ChatComponentText(player.getKey() + ":  " + player.getValue()));
        }
    }
}
